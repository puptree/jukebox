package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	
	@Test     // added by p.enkema
	public void testGetBurtBacharachAlbumSize() throws NoSuchFieldException, SecurityException {
		 BurtBacharach burtBacharach = new BurtBacharach();
		 ArrayList<Song> burtBacharachTracks = new ArrayList<Song>();
		 burtBacharachTracks = burtBacharach.getBurtBacharachSongs();
		 assertEquals(3, burtBacharachTracks.size());
	}

	@Test     // added by p.enkema
	public void testGetHerbAlpertAlbumSize() throws NoSuchFieldException, SecurityException {
		 HerbAlpert herbAlpert = new HerbAlpert();
		 ArrayList<Song> herbAlpertTracks = new ArrayList<Song>();
		 herbAlpertTracks = herbAlpert.getHerbAlpertSongs();
		 assertEquals(2, herbAlpertTracks.size());
	}
	
	@Test     // added by R.metzger to test Nobuo Uematsu song list size
	public void testGetNobuoUematsuAlbumSize() throws NoSuchFieldException, SecurityException {
		 NobuoUematsu nobuoUematsu = new NobuoUematsu();
		 ArrayList<Song> nobuoUematsuTracks = new ArrayList<Song>();
		 nobuoUematsuTracks = nobuoUematsu.getNobuoUematsuSongs();
		 assertEquals(2, nobuoUematsuTracks.size());
	}
	
	@Test     // added by R.metzger to test Queen song list size
	public void testGetQueenAlbumSize() throws NoSuchFieldException, SecurityException {
		 Queen queen = new Queen();
		 ArrayList<Song> queenTracks = new ArrayList<Song>();
		 queenTracks = queen.getQueenSongs();
		 assertEquals(2, queenTracks.size());
	}
	
	@Test     // added by R.metzger to test One Ok Rock song list size
	public void testGetOneOkRockAlbumSize() throws NoSuchFieldException, SecurityException {
		 OneOkRock oneOkRock = new OneOkRock();
		 ArrayList<Song> oneOkRockTracks = new ArrayList<Song>();
		 oneOkRockTracks = oneOkRock.getOneOkRockSongs();
		 assertEquals(2, oneOkRockTracks.size());
	}
	
}
