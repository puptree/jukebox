package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;

import music.artist.Queen;
import music.artist.ImagineDragons;
import music.artist.NobuoUematsu;
import music.artist.OneOkRock;
import music.artist.Adele;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;

public class RichardMetzger_Playlist {
	
	/* 
	 * songs from Queen, Nobuo Uematsu, One Ok Rock, Adele, and Imagine Dragons
	 */
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	/* Adds music from Nobuo Uematsu to the playlist
	 * 
	 */
	ArrayList<Song> nobuoUematsuTracks = new ArrayList<Song>();
	NobuoUematsu nobuoUematsu = new NobuoUematsu();
	nobuoUematsuTracks = nobuoUematsu.getNobuoUematsuSongs();
	
	playlist.add(nobuoUematsuTracks.get(0));
	playlist.add(nobuoUematsuTracks.get(1));

	/* Adds music from Queen to the play list
	 * 
	 */
	ArrayList<Song> queenTracks = new ArrayList<Song>();
	Queen queen = new Queen();
	queenTracks = queen.getQueenSongs();
	
	playlist.add(queenTracks.get(0));
	playlist.add(queenTracks.get(1));
	
	ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
	ImagineDragons imagineDragonsBand = new ImagineDragons();
    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
	
	playlist.add(imagineDragonsTracks.get(0));
	playlist.add(imagineDragonsTracks.get(1));
	playlist.add(imagineDragonsTracks.get(2));
	
	/* Adds music from  to the play list
	 * 
	 */
	ArrayList<Song> oneOkRockTracks = new ArrayList<Song>();
	OneOkRock oneOkRock = new OneOkRock();
	oneOkRockTracks = oneOkRock.getOneOkRockSongs();
	
	playlist.add(oneOkRockTracks.get(0));
	
	/* Adds music from Adele to the play list
	 * 
	 */
	ArrayList<Song> adeleTracks = new ArrayList<Song>();
	Adele adele = new Adele();
	adeleTracks = adele.getAdelesSongs();
	
	playlist.add(adeleTracks.get(0));
	playlist.add(adeleTracks.get(1));
	playlist.add(adeleTracks.get(2));
	
    return playlist;
	}

}
