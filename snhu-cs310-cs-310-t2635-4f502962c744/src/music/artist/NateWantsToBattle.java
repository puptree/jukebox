package music.artist;

import snhu.jukebox.playlist.Song;

import java.util.ArrayList;
public class NateWantsToBattle {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public NateWantsToBattle() { //create new artist list 
    }
    
    public ArrayList<Song> getNateWantsToBattleSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album and populate with below titles 
    	 Song track1 = new Song("Shape of You", "NateWantsToBattle");             //Create a song
         Song track2 = new Song("What's This?", "NateWantsToBattle"); //Create another song
         
         this.albumTracks.add(track1);                                          //Add the first song to NateWantsToBattle song list
         this.albumTracks.add(track2);                                          //Add the second song to NateWantsToBattle song list
         return albumTracks;                                                    //Return the songs for NateWantsToBattle in the form of an ArrayList
    }
}

