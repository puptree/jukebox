package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class NobuoUematsu {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public NobuoUematsu() {
    }
    
    public ArrayList<Song> getNobuoUematsuSongs() {
    	
   	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
   	 Song track1 = new Song("Answers", "Nobuo Uematsu");             //Create a song
        Song track2 = new Song("Dragonsong", "Nobuo Uematsu");         //Create another song
        this.albumTracks.add(track1);                                          //Add the first song to song list for the Nobuo Uematsu
        this.albumTracks.add(track2);                                          //Add the second song to song list for the Nobuo Uematsu
        return albumTracks;                                                    //Return the songs for the Nobuo Uematsu in the form of an ArrayList
   }

}
