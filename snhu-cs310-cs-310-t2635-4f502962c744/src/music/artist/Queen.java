package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Queen {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Queen() {
    }  
    
    public ArrayList<Song> getQueenSongs() {
    	
    	albumTracks = new ArrayList<Song>();
    	Song track1 = new Song ("I Want To Break Free", "Queen" );  //Instantiate the album so we can populate it below
    	Song track2 = new Song ("Bohemian Rhapsody", "Queen");  	//Create a song
    	this.albumTracks.add(track1);								//Add the first song to song list for Queen
    	this.albumTracks.add(track2);								//Add the second song to song list for Queen
    	return albumTracks;
    }
    

}
