package music.artist;

import snhu.jukebox.playlist.Song;

import java.util.ArrayList;
public class VoicePlay { //create new artist list 
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public VoicePlay() {
    }
    
    public ArrayList<Song> getVoicePlaySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album and populate with below titles 
    	 Song track1 = new Song("Kidnap the Sandy Claws", "VoicePlay");             //Create a song
         Song track2 = new Song("Hoist the Colours", "VoicePlay"); //Create another song
         Song track3 = new Song("Panic at the Disco part 1", "VoicePlay");
         this.albumTracks.add(track1);                                          //Add the first song to VoicePlay song list
         this.albumTracks.add(track2);                                          //Add the second song to VoicePlay song list
         return albumTracks;                                                    //Return the songs for VoicePlay in the form of an ArrayList
    }
}

