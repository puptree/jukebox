package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class OneOkRock {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public OneOkRock() {
    }
    
    public ArrayList<Song> getOneOkRockSongs() {
    	
   	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
   	 Song track1 = new Song ("Broken Heart of Gold", "One Ok Rock");         //Create a song
   	 Song track2 = new Song ("Renegades", "One Ok Rock");					//Create a second song
     this.albumTracks.add(track1);                                          //Add the first song to song list for One Ok Rock
     this.albumTracks.add(track2);											//Add the second song to song list for One Ok Rock
     return albumTracks;                                                    //Return the songs for One Ok Rock in the form of an ArrayList
   }

}
